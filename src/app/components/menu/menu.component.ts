import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  rutas = [
    {
      name: 'home',
      path: '/home'
    },
    {
      name: 'contact',
      path: '/contact'
    },
    {
      name: 'about',
      path: '/about'
    },
    {
      name: 'post',
      path: '/posts'
    },
    {
      name: 'prueba 2',
      path: '/prueba2'
    },
    {
      name: 'pruebaPersonal',
      path: '/pruebaPersonal'
    }

  ];

  constructor() { }

  ngOnInit() {
  }

}
