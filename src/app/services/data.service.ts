import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getPost() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts');
  }
  getPost2() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        tap(
          posts => {
            console.log(posts);
          }));
  }

  getJSON() {
    return this.http.get('../../assets/data.json');
  }

}


