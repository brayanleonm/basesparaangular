import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
// import { PostsComponent } from './pages/posts/posts.component';


const routes: Routes = [
  {
    component: AboutComponent,
    path: 'about'
  },
  {
    component: ContactComponent,
    path: 'contact'
  },
  {
    component: HomeComponent,
    path: 'home'
  },
  {
    loadChildren: './pages/posts/posts.module#PostsModule',
    path: 'posts'
  },
  {
    loadChildren: './pages/prueba/prueba.module#PruebaModule',
    path: 'prueba2'
  },
  {
    loadChildren: './pages/prueba-lazy/prueba-lazy.module#PruebaLazyModule',
    path: 'pruebaPersonal'
  },
  {

    path: '**',
    redirectTo: 'home'

  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
