import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.css']
})
export class PruebaComponent implements OnInit {
  infs: any = [];
  constructor(private dataservice: DataService) { }

  ngOnInit() {
    this.dataservice.getPost().subscribe(posts => {
      console.log(posts);
      this.infs = posts;
    });
  }

}
