import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() inf;
  @Output() clickPost = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  ngClick() {
    console.log('esto esta en hijo ' + this.inf.id);
    this.clickPost.emit(this.inf.id);
  }

}
