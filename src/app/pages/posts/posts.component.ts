import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  infs: any = [];
  constructor(private dataservice: DataService) { }

  ngOnInit() {
    this.infs = this.dataservice.getPost2();
  }

  escuchaClick(event) {
    console.log('esto llega ' + event);
  }

}
