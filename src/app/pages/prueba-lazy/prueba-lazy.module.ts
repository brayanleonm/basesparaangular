import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PruebaLazyRoutingModule } from './prueba-lazy-routing.module';
import { PruebaLazyComponent } from './prueba-lazy.component';


@NgModule({
  declarations: [PruebaLazyComponent],
  exports: [PruebaLazyComponent],
  imports: [
    CommonModule,
    PruebaLazyRoutingModule
  ]
})
export class PruebaLazyModule { }
