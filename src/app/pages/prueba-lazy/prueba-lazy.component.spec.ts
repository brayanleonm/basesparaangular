import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaLazyComponent } from './prueba-lazy.component';

describe('PruebaLazyComponent', () => {
  let component: PruebaLazyComponent;
  let fixture: ComponentFixture<PruebaLazyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PruebaLazyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaLazyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
