import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PruebaLazyComponent } from './prueba-lazy.component';


const routes: Routes = [
  {
    path: '',
    component: PruebaLazyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PruebaLazyRoutingModule { }
